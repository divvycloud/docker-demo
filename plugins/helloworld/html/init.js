let h = window.divvy.hyperapp.h;
let app = window.divvy.hyperapp.app;

const actions = {
    down: value => state => ({ count: state.count - value }),
    up: value => state => ({ count: state.count + value })
};

const view = (state, actions) =>
      h("div", {}, [
          h("h1", {}, state.count),
          h("button", { onclick: () => actions.down(1) }, "-"),
          h("button", { onclick: () => actions.up(1) }, "+")
      ]);


window.divvy.registerPage({
    label: "Counter",
    route: "counter?id",
    name: "counter",
    base: "counter",
    order: 60,
    description: "We count things here",
    oncreate: function(el, urlParams){
        let state = {
            count: urlParams.id || 0
        };
        app(state, actions, view, el);
    }
});
