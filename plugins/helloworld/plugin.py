from DivvyBlueprints.v2 import Blueprint
from DivvyPlugins.plugin_helpers import register_api_blueprint, unregister_api_blueprints
from DivvyPlugins.plugin_metadata import PluginMetadata


# This is our Flask blueprint for exposing the API call
blueprint = Blueprint('helloworld', __name__)


class metadata(PluginMetadata):
    """
    Information about this plugin
    """
    version = '1.2'
    last_updated_date = '2016-03-26'
    author = 'Divvy Cloud Corp.'
    nickname = 'helloworld'
    support_email = 'support@divvycloud.com'
    support_url = 'http://support.divvycloud.com'
    main_url = 'http://www.divvycloud.com'
    type = 'compliance'


def load():
    register_api_blueprint(blueprint)


def unload():
    unregister_api_blueprints()
