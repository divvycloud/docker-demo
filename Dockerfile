FROM ubuntu:16.04 AS build

# Avoid error messages from apt during image build
ARG DEBIAN_FRONTEND=noninteractive


RUN \
apt-get update && \
apt-get install -y wget curl

# Build application
ENV HOME=/opt/app/

WORKDIR /opt/app

COPY plugins plugins/

# Build egg files here
# ...
# ...


FROM divvycloud/divvycloud:latest

# Copy egg files from build container
COPY --from=build /opt/app/plugins .
